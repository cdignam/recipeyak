# Generated by Django 1.11.7 on 2017-12-02 18:57

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("recipeyak", "0019_auto_20171202_1827")]

    operations = [
        migrations.AddField(
            model_name="recipe",
            name="cart_additions",
            field=models.IntegerField(default=0, editable=False),
        )
    ]
