# Generated by Django 3.2.9 on 2022-12-18 23:34

import django.db.models.deletion
import django.utils.timezone
from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("recipeyak", "0104_upload_background_url"),
    ]

    operations = [
        migrations.CreateModel(
            name="ScheduleEvent",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created", models.DateTimeField(default=django.utils.timezone.now)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("before_on", models.DateField()),
                ("after_on", models.DateField()),
                (
                    "actor",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "scheduled_recipe",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="recipeyak.scheduledrecipe",
                    ),
                ),
            ],
            options={
                "db_table": "schedule_event",
                "ordering": ["-created"],
            },
        ),
    ]
