from typing import Any

import pytest

from recipeyak.api.base.serialization import UnexpectedDatabaseAccess
from recipeyak.api.serializers.recipe import RecipeSerializer
from recipeyak.models import Recipe


@pytest.mark.django_db
def test_db_blocker_warn_still_calls_db(settings: Any, recipe: Recipe) -> None:
    """
    Shouldn't fail, but still fetch from database with a warning logged
    """
    settings.ERROR_ON_SERIALIZER_DB_ACCESS = False
    queryset = Recipe.objects.all()

    assert RecipeSerializer(queryset, many=True).data


@pytest.mark.django_db
def test_db_blocker_fails_with_proper_settings(settings: Any, recipe: Recipe) -> None:
    """
    When the proper config setting is specified, database access should raise
    an exception inside a serializer.
    """
    settings.ERROR_ON_SERIALIZER_DB_ACCESS = True
    queryset = Recipe.objects.all()

    with pytest.raises(UnexpectedDatabaseAccess):
        RecipeSerializer(queryset, many=True).data  # noqa: B018
