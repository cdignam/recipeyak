/** from: https://lucide.dev/icon/settings-2 */
export const IconSettings = () => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={16}
    height={16}
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth={2}
    strokeLinecap="round"
    strokeLinejoin="round"
  >
    <path d="M20 7h-9M14 17H5" />
    <circle cx={17} cy={17} r={3} />
    <circle cx={7} cy={7} r={3} />
  </svg>
)
