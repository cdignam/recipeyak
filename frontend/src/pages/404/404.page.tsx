export const NotFoundPage = () => (
  <section>
    <h1 className="text-center text-[10rem] font-bold">404</h1>
    <p className="text-center text-[3rem]">Nothing here 🌵</p>
  </section>
)
