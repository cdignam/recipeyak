import { styled } from "@/theme"

export const FormControl = styled.div`
  font-size: 1rem;
  position: relative;
  text-align: left;
`
